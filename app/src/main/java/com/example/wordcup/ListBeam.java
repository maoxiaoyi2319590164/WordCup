package com.example.wordcup;

import java.util.List;

/**
 * Created by Administrator on 2020/6/2.
 */

class ListBeam {

    /**
     * msg : 成功
     * success : true
     * list : [{"id":1,"teamid":"A1","group":"A","teamname":"俄罗斯","teampic":"elos.png"},{"id":2,"teamid":"A2","group":"A","teamname":"乌拉圭","teampic":"wulagui.png"},{"id":3,"teamid":"A3","group":"A","teamname":"埃及","teampic":"aiji.png"},{"id":4,"teamid":"A4","group":"A","teamname":"沙特阿拉伯","teampic":"shatealabo.png"}]
     */

    private String msg;
    private boolean success;
    private List<Team> list;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Team> getList() {
        return list;
    }

    public void setList(List<Team> list) {
        this.list = list;
    }

    public static class Team {
        /**
         * id : 1
         * teamid : A1
         * group : A
         * teamname : 俄罗斯
         * teampic : elos.png
         */

        private int id;
        private String teamid;
        private String group;
        private String teamname;
        private String teampic;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTeamid() {
            return teamid;
        }

        public void setTeamid(String teamid) {
            this.teamid = teamid;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getTeamname() {
            return teamname;
        }

        public void setTeamname(String teamname) {
            this.teamname = teamname;
        }

        public String getTeampic() {
            return teampic;
        }

        public void setTeampic(String teampic) {
            this.teampic = teampic;
        }
    }
}
