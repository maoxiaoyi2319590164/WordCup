package com.example.wordcup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.wordcup.R;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    ListView mLvTeams;
    List<ListBeam.Team> teamsList = new ArrayList<>();
    MyAdapter adapter = new MyAdapter();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();//获取控件
        initData();//获取数据更新界面
    }

    private void initData() {
        //访问网络
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .build();
        final Request request = new Request.Builder()
                .url("http://192.168.1.104:8080/worldcupserver/teams/list")
                .post(requestBody)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, "网络访问失败", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Gson gson = new Gson();
                String jsonStr = response.body().string();
                ListBeam listBeam = gson.fromJson(jsonStr, ListBeam.class);
                teamsList = listBeam.getList();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void initView() {
        mLvTeams = findViewById(R.id.Iv_teams);
        mLvTeams.setAdapter(adapter);

    }
    class MyAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return teamsList.size();
        }

        @Override
        public Object getItem(int position) {
            return teamsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return teamsList.get(position).getId();
        }

        @Override
        public View getView(int position, View convertview, ViewGroup parent) {
            View view = View.inflate(MainActivity.this, R.layout.list_item, null);
            TextView tvId = view.findViewById(R.id.tv_team_id);
            TextView tvName = view.findViewById(R.id.tv_team_name);
            ImageView  imageView = view.findViewById(R.id.iv_team);
            tvId.setText(teamsList.get(position).getTeamid());
            tvName.setText(teamsList.get(position).getTeamname());
            Picasso.with(MainActivity.this)
                    .load("http://192.168.1.104:8080/worldcupserver/"
                            +teamsList.get(position).getTeampic())
                    .into(imageView);
            return view;
        }
    }
}

